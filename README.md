# Entity display tooltip

## Info
Provides the possibility to convert the help text of a field into a tooltip on the entity display.

If enabled for a field, the help text at Administration » Structure » Content types » << Entity >> » Manage fields » << Field settings >> is tranfered as the value to the title attribute of the fields label div-element and an info icon is appended.

Now, you can hover over the info icon to display the help text.

[Drupal Page](https://www.drupal.org/project/entity_display_tooltip)

[Report Issues](https://drupal.org/project/issues/entity_display_tooltip)

We used [Field description tooltip](https://www.drupal.org/project/field_description_tooltip) as a starting point.


## Configuration

### Permissions
Configure the user permissions in Administration » Configuration » User interface.
### Tooltip fields
Administration » Configuration » User Interface: Entity display tooltip settings  
You can ether choose to display tooltips for every field or choose which fields in which bundles should display a tooltip.


## Maintainer

* [Philipp Eisenhuth](https://drupal.org/u/philippeisenhuth)
* [Myriel Fichtner](https://drupal.org/u/selphie)
* [Robert Nasarek](https://drupal.org/u/rnsrk)
* [Gustavo Riva](https://drupal.org/u/gusriva)

