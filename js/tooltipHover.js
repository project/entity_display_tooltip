(function ($, Drupal, once) {
  'use strict';

  /**
   * The tooltip info box plugin.
   * This is used for displaying the actual Tooltip for the corresponding Fields when you hover over the info-icon.
   */
  Drupal.behaviors.entityDisplayTooltipHover = {
    attach: function (context, settings) {
      once('entityDisplayTooltipHover', 'html', context).forEach(function (element) {
        $('div[data-description-tooltip="1"] div.field__label img').hover( function () {
          $(this).closest('div[data-description-tooltip="1"]').find('.tooltip-window').toggleClass('active');
        })
      });
    }
  };
})(jQuery, Drupal, once);
