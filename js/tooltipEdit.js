(function ($, Drupal, once) {
    'use strict';
    /**
     * The tooltip info box plugin.
     */
    Drupal.behaviors.viewTooltipEdit = {
        attach: function (context, settings) {
            once('viewTooltipEdit', 'html', context).forEach(function (element) {
                console.log(settings);
                let tooltipFields = $('[data-description-tooltip="1"]', context);

                function tooltipHover (element) {
                    $(element).append('<div class="test"> Hier bin ich </div>');
                    //$(element).find('div.test').last().remove();
                }

                // Check if there are any fields that are configured as tooltip.
                if (tooltipFields.length) {
                    tooltipFields.each(function () {
                        let description = $(this).find('[data-drupal-field-elements="description"], [class*="description"]');

                        // Check if there is a description available in order to start the
                        // js manipulations.
                        if (description.length) {
                            description.each(function () {
                                // Get the description text to be prepared as tooltip.
                                let tooltipText = $(this).html().trim();
                                let lineBreak = '<br />';

                                // Remove the description text and move it to the "title" attribute.
                                $(this).html('<img width="20" src="/' + settings.viewTooltip.img + '" />');
                                $(this).attr('title', tooltipText);
                                $(this).append('<div class="tooltip-box-parent"><div class="tooltip-box">' + tooltipText + '</div></div>');
                                console.log(  $(this), "divdesc");

                                // we can't select the pseudo elements here?
                                $(this).find('img').hover(
                                    //function(){$(this).find(':after')},
                                    function(){$(this).next().slideToggle()}
                                );

                                // Add the tooltip js trigger.
                                /*
                                $(this).tooltip({
                                    show: { effect: "slideDown" },
                                    // For any custom styling.
                                    tooltipClass: "description-tooltip",
                                    content: function () {
                                        let tooltipText = $(this).prop('title');
                                        // Convert default line breaks into html breaks.
                                        return tooltipText
                                            .replaceAll("\r\n", lineBreak)
                                            .replaceAll("\r", lineBreak)
                                            .replaceAll("\n", lineBreak)
                                    }
                                });
                                */
                            });
                        }
                    });
                }
            })
        }
    };
})(jQuery, Drupal, once);
