(function ($, Drupal, once) {
    'use strict';
    /**
     * The tooltip info box plugin.
     */
    Drupal.behaviors.entityDisplayTooltip = {
        attach: function (context, settings) {
            once('entityDisplayTooltipEdit', 'html', context).forEach(function (element) {

            let fields = settings.entityDisplayTooltipFields
            console.log(fields);
            
            for (const fieldId in fields) {
                let selector = '.field--name-' + fieldId + '> div.field__label'
                let titleField = $(selector)
                titleField.addClass('showTooltip');
                titleField.attr('title', fields[fieldId]);
            }
            })
        }
    }
})(jQuery, Drupal, once);
