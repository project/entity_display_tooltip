<?php

namespace Drupal\entity_display_tooltip\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the field description settings to convert into a tooltiop.
 */
class EntityDisplayTooltipConfigForm extends ConfigFormBase implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    $instance->entityDefinitionUpdateManager = $container->get('entity.definition_update_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_display_tooltip_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_display_tooltip.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Load the saved configuration (if available).
    $config = $this->config('entity_display_tooltip.settings');
    // Get all the available installed entity types.
    $entity_types = $this->entityDefinitionUpdateManager->getEntityTypes();
    $fieldable_interface = 'Drupal\Core\Entity\FieldableEntityInterface';

    $form['info'] = [
      '#markup' => '<p>' . $this->t('Set which entity form fields will convert the description into a tooltip.')
      . '</p><strong>' . $this->t('NOTE:') . ' </strong>'
      . $this->t('The entity display tooltip is only workable for the displays.') . '</p>',
    ];

    $form['tooltip_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Tolltip all?'),
      '#default_value' => $config->get('tooltip_all') ?? FALSE,
      '#description' => $this->t('Convert all entity field descriptions into a tooltip or not.'),
    ];

    foreach ($entity_types as $type) {
      // Do not show the non-fieldable entity types.
      if (!in_array($fieldable_interface, class_implements($type->getOriginalClass()))) {
        continue;
      }
      $type_id = $type->id();

      // Get the entity type bundles.
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($type_id);

      if (!$bundles) {
        continue;
      }
      $form[$type_id] = [
        '#type' => 'details',
        '#title' => $this->t('Entity type: %type', ['%type' => $type_id]),
        '#open' => TRUE,
      ];

      foreach ($bundles as $bundle => $label) {
        $form[$type_id][$bundle] = [
          '#type' => 'details',
          '#title' => $this->t('Bundle: %bundle', ['%bundle' => $label['label']]),
          '#open' => FALSE,
        ];

        // Get all the fields from the entity bundle.
        $fields = $this->entityFieldManager->getFieldDefinitions($type_id, $bundle);

        $count = $active = 0;
        foreach ($fields as $field) {
          // Do not count the read-only or no form options listed fields.
          if ($field->isReadOnly() || !$field->getDisplayOptions('form')) {
            continue;
          }
          $count++;

          // Generate the field name based on the params.
          $field_name = $type_id . ':' . $bundle . ':' . $field->getName();
          $form[$type_id][$bundle][$field_name] = [
            '#type' => 'checkbox',
            '#title' => $field->getLabel(),
            '#default_value' => $config->get($field_name) ?? FALSE,
          ];

          if (!empty($config->get($field_name))) {
            $active++;
          }
        }

        if (!$count) {
          $form[$type_id][$bundle][] = [
            '#markup' => $this->t('No available fields'),
          ];
        }

        // Make the details element open once there are any selected field.
        if ($active) {
          $form[$type_id][$bundle]['#open'] = TRUE;
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Clean the form values.
    $form_state->cleanValues();

    // Retrieve the configuration prepared to be saved.
    $settings = $this->configFactory->getEditable('entity_display_tooltip.settings');
    foreach ($form_state->getValues() as $name => $config) {
      $settings->set($name, $config);
    }
    $settings->save();

    parent::submitForm($form, $form_state);
  }

}
